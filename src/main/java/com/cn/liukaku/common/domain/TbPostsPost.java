package com.cn.liukaku.common.domain;

import java.io.Serializable;
import java.util.Date;

public class TbPostsPost  extends BaseDomain implements Serializable{

    private static final long serialVersionUID = 2888528343611720974L;

    private String postGuid;

    private String title;

    private Date timePublished;

    private String status;

    private String alias;

    private Short score;

    private Integer numberOfUpvotes;

    private Integer numberOfDownvotes;

    private Integer numberOfReads;

    private Integer numberOfShares;

    private Integer numberOfBookmarks;

    private Integer numberOfComments;

    private String rejectMsg;

    private String access;

    private String summary;

    private String main;

    private String authors;

    private String thumbImage;

    private String originalImages;

    private String images;

    private String fullSizeImages;

    private String tags;

    private String vTags;

    private String series;



    public String getPostGuid() {
        return postGuid;
    }

    public void setPostGuid(String postGuid) {
        this.postGuid = postGuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTimePublished() {
        return timePublished;
    }

    public void setTimePublished(Date timePublished) {
        this.timePublished = timePublished;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Short getScore() {
        return score;
    }

    public void setScore(Short score) {
        this.score = score;
    }

    public Integer getNumberOfUpvotes() {
        return numberOfUpvotes;
    }

    public void setNumberOfUpvotes(Integer numberOfUpvotes) {
        this.numberOfUpvotes = numberOfUpvotes;
    }

    public Integer getNumberOfDownvotes() {
        return numberOfDownvotes;
    }

    public void setNumberOfDownvotes(Integer numberOfDownvotes) {
        this.numberOfDownvotes = numberOfDownvotes;
    }

    public Integer getNumberOfReads() {
        return numberOfReads;
    }

    public void setNumberOfReads(Integer numberOfReads) {
        this.numberOfReads = numberOfReads;
    }

    public Integer getNumberOfShares() {
        return numberOfShares;
    }

    public void setNumberOfShares(Integer numberOfShares) {
        this.numberOfShares = numberOfShares;
    }

    public Integer getNumberOfBookmarks() {
        return numberOfBookmarks;
    }

    public void setNumberOfBookmarks(Integer numberOfBookmarks) {
        this.numberOfBookmarks = numberOfBookmarks;
    }

    public Integer getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(Integer numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public String getRejectMsg() {
        return rejectMsg;
    }

    public void setRejectMsg(String rejectMsg) {
        this.rejectMsg = rejectMsg;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getOriginalImages() {
        return originalImages;
    }

    public void setOriginalImages(String originalImages) {
        this.originalImages = originalImages;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getFullSizeImages() {
        return fullSizeImages;
    }

    public void setFullSizeImages(String fullSizeImages) {
        this.fullSizeImages = fullSizeImages;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getvTags() {
        return vTags;
    }

    public void setvTags(String vTags) {
        this.vTags = vTags;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}